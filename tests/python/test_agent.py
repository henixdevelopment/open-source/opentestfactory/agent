# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""agent unit tests."""

import asyncio
import sys
import unittest

from unittest.mock import MagicMock, patch, AsyncMock

from mock_windll import ctypes

import requests

import opentf.agent.agent


########################################################################
# Datasets


########################################################################
# Helpers


########################################################################


class TestAgent(unittest.TestCase):
    """Unit tests for agent."""

    # main

    def test_main_1(self):
        self.assertRaises(SystemExit, opentf.agent.agent.main)

    def test_main_2(self):
        args = ['', '--host', 'hOSt', '--tags', 'rOBOtFrameWorK']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, opentf.agent.agent.main)

    def test_main_3(self):
        args = ['', '--host', 'hOSt', '--tags', 'rOBOtFrameWorK', '--debug']
        with patch.object(sys, 'argv', args):
            self.assertRaises(SystemExit, opentf.agent.agent.main)

    def test_main_4(self):
        args = ['', '--host', 'http://', '--tags', 'rOBOtFrameWorK', '--debug']
        mock = MagicMock(return_value='Windows')
        with patch.object(sys, 'argv', args), patch('platform.system', mock):
            self.assertRaises(SystemExit, opentf.agent.agent.main)

    # _ensure_tags_valid

    def test_ensure_tags_valid_1(self):
        tags = ['linux', 'rOBOtFrameWorK']
        mock = MagicMock(return_value='Windows')
        with patch('platform.system', mock):
            self.assertRaises(SystemExit, opentf.agent.agent._ensure_tags_valid, tags)

    def test_ensure_tags_valid_2(self):
        tags = ['linux', 'rOBOtFrameWorK']
        mock = MagicMock(return_value='Linux')
        with patch('platform.system', mock):
            opentf.agent.agent._ensure_tags_valid(tags)

    def test_ensure_tags_valid_3(self):
        tags = ['rOBOtFrameWorK']
        mock = MagicMock(return_value='Linux')
        with patch('platform.system', mock):
            self.assertRaises(SystemExit, opentf.agent.agent._ensure_tags_valid, tags)

    def test_ensure_tags_valid_inception(self):
        tags = ['inception', 'linux']
        mock = MagicMock(return_value='Linux')
        with patch('platform.system', mock):
            self.assertRaises(SystemExit, opentf.agent.agent._ensure_tags_valid, tags)

    # _prepare_registration

    def test_prepare_registration_1(self):
        mockresponse = MagicMock()
        mockresponse.tags = 'linux,robotframework'
        opentf.agent.agent._prepare_registration(mockresponse)
        self.assertEqual(len(opentf.agent.agent.REGISTRATION['spec']['tags']), 2)

    def test_prepare_registration_windows_utf8(self):
        mockresponse = MagicMock()
        mockresponse.tags = 'windows,robotframework'
        mockresponse.encoding = 'utf-8'
        mock_warn = MagicMock()
        mock_plat = MagicMock(return_value='Windows')
        mock_gco = MagicMock(return_value=65001)
        ctypes.windll.kernel32.GetConsoleOutputCP = mock_gco
        with patch('logging.warning', mock_warn), patch('platform.system', mock_plat):
            opentf.agent.agent._prepare_registration(mockresponse)
        self.assertEqual(len(opentf.agent.agent.REGISTRATION['spec']['tags']), 2)
        mock_gco.assert_called_once()
        mock_warn.assert_not_called()

    def test_prepare_registration_windows_notutf8(self):
        mockresponse = MagicMock()
        mockresponse.tags = 'windows,robotframework'
        mockresponse.encoding = 'utf-8'
        mock_warn = MagicMock()
        mock_plat = MagicMock(return_value='Windows')
        mock_gco = MagicMock(return_value=850)
        ctypes.windll.kernel32.GetConsoleOutputCP = mock_gco
        with patch('logging.warning', mock_warn), patch('platform.system', mock_plat):
            opentf.agent.agent._prepare_registration(mockresponse)
        self.assertEqual(len(opentf.agent.agent.REGISTRATION['spec']['tags']), 2)
        mock_gco.assert_called_once()
        mock_warn.assert_called_once()

    def test_prepare_registration_linux_notutf8(self):
        mockresponse = MagicMock()
        mockresponse.tags = 'linux,robotframework'
        mockresponse.encoding = 'utf-8'
        mock_warn = MagicMock()
        mock_plat = MagicMock(return_value='Linux')
        with patch('logging.warning', mock_warn), patch(
            'platform.system', mock_plat
        ), patch('os.environ', {'LANG': 'C'}):
            opentf.agent.agent._prepare_registration(mockresponse)
        self.assertEqual(len(opentf.agent.agent.REGISTRATION['spec']['tags']), 2)
        mock_warn.assert_called_once()

    def test_prepare_registration_linux_utf8(self):
        mockresponse = MagicMock()
        mockresponse.tags = 'linux,robotframework'
        mockresponse.encoding = 'utf-8'
        mock_warn = MagicMock()
        mock_plat = MagicMock(return_value='Linux')
        with patch('logging.warning', mock_warn), patch(
            'platform.system', mock_plat
        ), patch('os.environ', {'LANG': 'C.UTF-8'}):
            opentf.agent.agent._prepare_registration(mockresponse)
        self.assertEqual(len(opentf.agent.agent.REGISTRATION['spec']['tags']), 2)
        mock_warn.assert_not_called()

    # _read_sslconfiguration

    def test_read_sslconfiguration_1(self):
        mock = MagicMock()
        mock.verify = 'True'
        self.assertTrue(opentf.agent.agent._read_sslconfiguration(mock))

    def test_read_sslconfiguration_2(self):
        mock = MagicMock()
        mock.verify = 'False'
        self.assertFalse(opentf.agent.agent._read_sslconfiguration(mock))

    # _handle_registration_error

    def test_handle_registration_error_1(self):
        mock = MagicMock()
        with patch('logging.error', mock):
            opentf.agent.agent._handle_registration_error(
                requests.exceptions.ProxyError(), 'hOsT'
            )
        self.assertEqual(mock.call_args_list[0][0][0], 'A proxy error occurred: %s.')

    def test_handle_registration_error_2(self):
        mock = MagicMock()
        with patch('logging.error', mock):
            opentf.agent.agent._handle_registration_error(
                requests.exceptions.SSLError(), 'hOsT'
            )
        self.assertEqual(mock.call_args_list[0][0][0], 'A SSL error occurred: %s.')

    def test_handle_registration_error_3(self):
        mock = MagicMock()
        with patch('logging.error', mock):
            opentf.agent.agent._handle_registration_error(
                requests.exceptions.ConnectionError(), 'hOsT'
            )
        self.assertTrue(
            mock.call_args_list[0][0][0].startswith('Could not reach the orchestrator')
        )

    def test_handle_registration_error_4(self):
        mock = MagicMock()
        with patch('logging.error', mock):
            opentf.agent.agent._handle_registration_error(Exception(), 'hOsT')
        self.assertTrue(
            mock.call_args_list[0][0][0].startswith('Failed to register to server: %s')
        )

    # register_and_handle

    def test_register_and_handle_1(self):
        args = MagicMock()
        args.path_prefix = 'pAth_PreFix/'
        args.host = 'hOsT'
        args.port = 9999
        args.retry = 1111
        args.polling_delay = 222
        mock = MagicMock()
        mock.side_effect = requests.exceptions.ProxyError('Oh nooooo!')
        with patch('opentf.agent.agent.post', mock):
            result = opentf.agent.agent.register_and_handle(args, None, True)
        self.assertEqual(result, opentf.agent.agent.STATUS_REGISTRATION_FAILED)

    def test_register_and_handle_2(self):
        args = MagicMock()
        args.path_prefix = 'pAth_PreFix/'
        args.host = 'hOsT'
        args.port = 9999
        args.retry = 1111
        args.polling_delay = 222
        mockresponse = requests.Response()
        mockresponse.json = lambda: {}
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.agent.agent.post', mock):
            result = opentf.agent.agent.register_and_handle(args, None, True)
        self.assertEqual(result, opentf.agent.agent.STATUS_REGISTRATION_FAILED)

    def test_register_and_handle_3(self):
        args = MagicMock()
        args.path_prefix = 'pAth_PreFix/'
        args.host = 'hOsT'
        args.port = 9999
        args.retry = 1111
        args.polling_delay = 222
        mockresponse = requests.Response()
        mockresponse.json = lambda: {'details': {'uuid': 'a12b54'}}
        mock = MagicMock(return_value=mockresponse)
        mock_info = MagicMock()
        with patch('opentf.agent.agent.post', mock), patch('logging.info', mock_info):
            result = opentf.agent.agent.register_and_handle(args, None, True)
        self.assertEqual(result, opentf.agent.agent.STATUS_EXCEPTION)
        infos = {arg[0][0] for arg in mock_info.call_args_list}
        self.assertFalse('OpenTestFactory Orchestrator version %s.' in infos)

    def test_register_and_handle_4(self):
        args = MagicMock()
        args.path_prefix = 'pAth_PreFix/'
        args.host = 'hOsT'
        args.port = 9999
        args.retry = 1111
        args.polling_delay = 222
        mockresponse = requests.Response()
        mockresponse.json = lambda: {
            'details': {'uuid': 'a12b54', 'version': 'vERSioN'}
        }
        mock = MagicMock(return_value=mockresponse)
        mock_info = MagicMock()
        with patch('opentf.agent.agent.post', mock), patch('logging.info', mock_info):
            result = opentf.agent.agent.register_and_handle(args, None, True)
        self.assertEqual(result, opentf.agent.agent.STATUS_EXCEPTION)
        infos = {arg[0][0] for arg in mock_info.call_args_list}
        self.assertTrue('OpenTestFactory Orchestrator version %s.' in infos)

    # _ensure_namespaces_valid

    def test_ensure_namespaces_valid_missing(self):
        self.assertRaises(SystemExit, opentf.agent.agent._ensure_namespaces_valid, '')

    def test_ensure_namespaces_valid_mixedstar(self):
        self.assertRaises(
            SystemExit, opentf.agent.agent._ensure_namespaces_valid, 'foo,*'
        )

    def test_ensure_namespaces_valid_star(self):
        opentf.agent.agent._ensure_namespaces_valid('*')

    def test_ensure_namespaces_valid_singular(self):
        opentf.agent.agent._ensure_namespaces_valid('foo')

    def test_ensure_namespaces_valid_multiple(self):
        opentf.agent.agent._ensure_namespaces_valid('foo,bar')

    def test_ensure_namespaces_valid_multiple_notallowed(self):
        self.assertRaises(
            SystemExit, opentf.agent.agent._ensure_namespaces_valid, 'foo+,bar'
        )

    # post

    def test_post_badssl(self):
        mock_requests = MagicMock()
        mock_requests.post = MagicMock(
            side_effect=requests.exceptions.SSLError('sslError')
        )
        mock_requests.exceptions = requests.exceptions
        mock_logging = MagicMock()
        mock_logging.debug = MagicMock()
        with patch('opentf.agent.agent.requests', mock_requests), patch(
            'opentf.agent.agent.logging', mock_logging
        ):
            self.assertRaises(
                requests.exceptions.SSLError,
                opentf.agent.agent.post,
                'eNDpOINt',
                {},
                {},
                1,
                1,
                False,
            )
        mock_logging.debug.assert_called_once()

    def test_post_badproxy(self):
        mock_requests = MagicMock()
        mock_requests.post = MagicMock(
            side_effect=requests.exceptions.ProxyError('sslError')
        )
        mock_requests.exceptions = requests.exceptions
        mock_logging = MagicMock()
        mock_logging.debug = MagicMock()
        with patch('opentf.agent.agent.requests', mock_requests), patch(
            'opentf.agent.agent.logging', mock_logging
        ):
            self.assertRaises(
                requests.exceptions.ProxyError,
                opentf.agent.agent.post,
                'eNDpOINt',
                {},
                {},
                1,
                1,
                False,
            )
        mock_logging.debug.assert_called_once()

    def test_post_ce(self):
        mock_requests = MagicMock()
        mock_requests.post = MagicMock(
            side_effect=requests.exceptions.ConnectionError('sslError')
        )
        mock_requests.exceptions = requests.exceptions
        mock_logging = MagicMock()
        mock_logging.debug = MagicMock()
        with patch('opentf.agent.agent.requests', mock_requests), patch(
            'opentf.agent.agent.logging', mock_logging
        ):
            self.assertRaises(
                requests.exceptions.ConnectionError,
                opentf.agent.agent.post,
                'eNDpOINt',
                {},
                {},
                1,
                1,
                False,
            )
        mock_logging.debug.assert_called_once()

    def test_post_other(self):
        mock_requests = MagicMock()
        mock_requests.post = MagicMock(side_effect=Exception('ooOops'))
        mock_requests.exceptions = requests.exceptions
        mock_logging = MagicMock()
        mock_logging.debug = MagicMock()
        with patch('opentf.agent.agent.requests', mock_requests), patch(
            'opentf.agent.agent.logging', mock_logging
        ):
            self.assertRaises(
                Exception,
                opentf.agent.agent.post,
                'eNDpOINt',
                {},
                {},
                1,
                1,
                False,
            )
        mock_logging.debug.assert_not_called()

    @patch('requests.get')
    @patch('requests.post')
    @patch('asyncio.create_subprocess_shell')
    def test_command_execution(self, mock_subprocess, mock_post, mock_get):
        # Setup mocks
        opentf.agent.agent.REGISTRATION['spec']['workspace_dir'] = 'WorsksPacED_ir'
        mock_process = AsyncMock()
        mock_process.stdout.readline = AsyncMock(
            side_effect=[b"line1\n", b"line2\n", b""]
        )
        mock_process.stderr.readline = AsyncMock(side_effect=[b"error1\n", b""])
        mock_process.returncode = 0
        mock_subprocess.return_value = mock_process

        mock_get.return_value.json.return_value = {}
        mock_get.return_value.status_code = 200

        mock_post.return_value.status_code = 200

        # Test data
        agent_id = "test_agent"
        command = {"command": "echo 'Hello, World!'"}
        server = "http://localhost"
        headers = {"Authorization": "Bearer test"}
        verify = True

        # Run the coroutine
        loop = asyncio.get_event_loop()
        loop.run_until_complete(
            opentf.agent.agent.async_process_exec(
                agent_id, command, server, headers, verify
            )
        )

        # Assertions
        mock_subprocess.assert_called_once_with(
            command['command'],
            cwd=opentf.agent.agent.REGISTRATION['spec']['workspace_dir'],
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        )
        self.assertEqual(mock_process.stdout.readline.await_count, 3)
        self.assertEqual(mock_process.stderr.readline.await_count, 2)
        mock_post.assert_called()

    @patch('asyncio.sleep')
    @patch('requests.get')
    @patch('requests.post')
    @patch('asyncio.create_subprocess_shell')
    def test_command_cancellation(
        self, mock_subprocess, mock_post, mock_get, mock_sleep
    ):
        # Setup mocks
        opentf.agent.agent.REGISTRATION['spec']['workspace_dir'] = 'WorsksPacED_ir'
        mock_sleep = AsyncMock()
        mock_process = AsyncMock()
        mock_process.stdout.readline = AsyncMock(
            side_effect=[b"line1\n", b"", asyncio.Future()]
        )
        mock_process.stderr.readline = AsyncMock(side_effect=[b"", asyncio.Future()])
        mock_process.terminate = MagicMock()
        mock_process.returncode = None
        mock_subprocess.return_value = mock_process

        cancel_response = MagicMock()
        cancel_response.json.return_value = {'details': {'kind': 'cancel'}}
        cancel_response.status_code = 200
        mock_get.side_effect = [
            MagicMock(status_code=200, json=lambda: {}),
            cancel_response,
        ]

        mock_post.return_value.status_code = 200

        # Test data
        agent_id = "test_agent"
        command = {"command": "long_running_command"}
        server = "http://localhost"
        headers = {"Authorization": "Bearer test"}
        verify = True

        # Run the coroutine
        loop = asyncio.get_event_loop()
        loop.run_until_complete(
            opentf.agent.agent.async_process_exec(
                agent_id, command, server, headers, verify
            )
        )

        # Assertions
        mock_subprocess.assert_called_once()
        mock_process.terminate.assert_called_once()
        mock_post.assert_called()
