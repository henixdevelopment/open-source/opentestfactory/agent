# CHANGELOG


## 1.8.0

- Improved recovery on communication errors

## 1.7.0

- Improved logs
- Fixing incorrect re-registration

## 1.6.0

- Adding namespace support via the `--namespaces` command line option

## 1.5.0

- Adding `--version` command line option
- Adding a warning if the current code page is not 65001 on Windows
- Improved startup messages

## 1.4.0

- Improved OS tag checks

## 1.3.2

- Fixing README
- Fixing packaging

## 1.3.1

- Fixing premature logs

## 1.3.0

- Adding `--verify` command line option to disable SSL checks or specify a certificates chain

## 1.2.0

- Failed `put-file` functions now cause an ExecutionError
- Improved debug mode
- Improved logging

## 1.1.2

- Fixing typo

## 1.1.0

- Handling `put-file` functions

## 1.0.2

- Improved help message
- The project URL was incorrect

## 1.0.1

- Ensure an OS is specified in `--tags`
- Strip trailing slash on host
