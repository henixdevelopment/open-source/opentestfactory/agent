# opentf-agent

This package is part of the OpenTestFactory initiative.

## Overview

An agent is a process that runs on an execution environment.  That process will contact the orchestrator at regular intervals, looking for some orders to execute. If there is a pending order, the agent will do as asked and send the result back to the orchestrator.

The agent documentation is part of the
[OpenTestFactory orchestrator](https://opentestfactory.org/guides) documentation, in the **Guides** section.

## License

Copyright 2021, 2023 Henix, henix.fr

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
